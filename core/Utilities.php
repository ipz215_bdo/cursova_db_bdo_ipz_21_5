<?php

namespace core;

class Utilities
{
    public static function filterArray($array, $keys)
    {
        $result = [];
        foreach ($keys as $key) {
            if (isset($array[$key])) {
                $result[$key] = $array[$key];
            }
        }
        return $result;
    }

    public static function Encrypt($data)
    {
        return md5($data);
    }
}
