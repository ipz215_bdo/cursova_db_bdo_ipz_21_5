<?php

namespace core;

/**
 * Класс для роботи з базою даних
 */
class DB
{
    protected $pdo;
    public function __construct($host, $login, $password, $db_name)
    {
        
        $this->pdo = new \PDO("mysql:host={$host};dbname={$db_name}", $login, $password);
    }
    public function select($tableName, $fieldList = "*", $conditionArray = null, $orderBy = null)
    {
        if (is_string($fieldList))
            $fieldListString = $fieldList;
        if (is_array($fieldList))
            $fieldListString = implode(", ", $fieldList);

        $whereValues = "";
        if (is_array($conditionArray)) {
            $keys = [];
            foreach ($conditionArray as $key => $value) {
                $keys[] = "{$key} = :{$key}";
            }
            $whereValues = "WHERE " . implode(" AND ", $keys);
        }
        if (!empty($orderBy)) {
            $whereValues .= " {$orderBy}";
        }
        $sql = $this->pdo->prepare("SELECT {$fieldListString} FROM {$tableName} {$whereValues}");
        $sql->execute($conditionArray);
        return $sql->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function update($tableName, $fieldList, $conditionArray = null)
    {
        $fieldListString = "";
        if (is_array($fieldList)) {
            $keys = [];
            foreach ($fieldList as $key => $value) {
                $keys[] = "{$key} = :{$key}";
            }
            $fieldListString = implode(", ", $keys);
        }
        $whereValues = "";
        if (is_array($conditionArray)) {
            $keys = [];
            foreach ($conditionArray as $key => $value) {
                $keys[] = "{$key} = :{$key}";
            }
            $whereValues = "WHERE " . implode(" AND ", $keys);
        }
        $sql = $this->pdo->prepare("UPDATE {$tableName} SET {$fieldListString} {$whereValues}");
        $sql->execute(array_merge($fieldList, $conditionArray));
        return $sql->rowCount();
    }

    public function insert($tableName, $fieldList)
    {
        $fieldListString = "";
        if (is_array($fieldList)) {
            $keys = [];
            foreach ($fieldList as $key => $value) {
                $keys[] = "{$key} = :{$key}";
            }
            $fieldListString = implode(", ", $keys);
        }
        $sql = $this->pdo->prepare("INSERT INTO {$tableName} SET {$fieldListString}");
        $sql->execute($fieldList);
        return $this->pdo->lastInsertId();
    }


    public function delete($tableName, $conditionArray = null)
    {
        $whereValues = "";
        if (is_array($conditionArray)) {
            $keys = [];
            foreach ($conditionArray as $key => $value) {
                $keys[] = "{$key} = :{$key}";
            }
            $whereValues = "WHERE " . implode(" AND ", $keys);
        }
        $sql = $this->pdo->prepare("DELETE FROM {$tableName} {$whereValues}");
        $sql->execute($conditionArray);
        return $sql->rowCount();
    }

}
