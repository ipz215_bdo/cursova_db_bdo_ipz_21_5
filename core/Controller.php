<?php

namespace core;

class Controller
{
    protected $viewPath;
    protected $moduleName;
    protected $actionName;
    public function __construct()
    {
        $this->moduleName = \core\Core::getInstance()->app['moduleName'];
        $this->actionName = \core\Core::getInstance()->app['actionName'];
        $this->viewPath = "views/{$this->moduleName}/{$this->actionName}.php";
    }
    public function render($viewPath = null, $params = null)
    {
        if (empty($viewPath)) {
            $viewPath = $this->viewPath;
        }
        $tmp = new \core\Template($viewPath);
        if (!empty($params)) {
            $tmp->setParams($params);
        }
        return $tmp->getHtml();
    }
    
    public function renderPath($file)
    {
        $path = "views/{$this->moduleName}/{$file}.php";
        $tmp = new \core\Template($path);
        if (!empty($params)) {
            $tmp->setParams($params);
        }
        return $tmp->getHtml();
    }
}
