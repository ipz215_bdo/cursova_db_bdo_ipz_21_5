<?php

namespace core;

class Core
{
    private static $instance = null;
    public $app;
    public DB $db;
    public $requestMethod;
    private function __construct()
    {
        $this->app = [];
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function Initialize()
    {
        session_start();
        $this->db = new DB(DB_HOST, DB_LOGIN, DB_PASSWORD, DB_NAME);
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    }
    public function Run()
    {
        $route = $_GET['route'];
        $routeParts = explode('/', $route);
        $moduleName = strToLower(array_shift($routeParts));
        $actionName = strToLower(array_shift($routeParts));
        if (empty($moduleName)) {
            $moduleName = 'main';
        }
        if (empty($actionName)) {
            $actionName = 'index';
        }
        $this->app['moduleName'] = $moduleName;
        $this->app['actionName'] = $actionName;

        $controllerName = '\\controllers\\' . ucfirst($moduleName) . 'Controller';
        $controllerActionName = $actionName . 'Action';
        $error = 200;
        if (class_exists($controllerName)) {
            $controller = new $controllerName();

            if (method_exists($controller, $controllerActionName)) {
                $this->app['actionResult'] =  $controller->$controllerActionName($routeParts);
            } else {
                $error = 404;
            }
        } else {
            $error = 404;
        }
        $statusCode = intval($error / 100);
        if ($statusCode == 4 || $statusCode == 5 || $error != 200) {
            $controller = new \controllers\MainController();
            $controller->errorAction($error);
        }
    }

    public function Done()
    {
        $pathToLayout = 'themes/light/layout.php';
        $tmp = new \core\Template($pathToLayout);
        $tmp->setParam('content', $this->app['actionResult']);
        $tmp->setParam('categories', \models\Categories::getCategories());
        $html = $tmp->getHtml();
        echo $html;
    }
    public function redirect($url)
    {
        header("Location: {$url}");
        exit;
    }
}
