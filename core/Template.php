<?php

namespace core;

use Exception;

class Template
{
    protected $path;
    protected $params;
    public function __construct($path)
    {
        $this->path = $path;
        $this->params = [];
    }
    public function setParam($key, $value)
    {
        $this->params[$key] = $value;
    }
    public function setParams($params)
    {
        foreach($params as $key => $value) {
            $this->setParam($key, $value);
        }
    }
    public function getHtml()
    {
        if (!is_file($this->path)) {
            throw new Exception("Not found", 404);
        }
        ob_start();
        extract($this->params);
        include($this->path);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}