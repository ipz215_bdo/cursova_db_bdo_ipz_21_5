<?php

namespace models;

use core\Utilities;

class User
{
    protected static $tableName = 'users';
    public static function addUser($login, $password, $lastName, $firstName, $phone = null, $avatar = null)
    {
        \core\Core::getInstance()->db->insert(
            self::$tableName,
            [
                'login' => $login,
                'password' => $password,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'phone' => $phone,
                'avatar' => $avatar
            ]
        );
    }
    public static function updateUser($id, $updateArray)
    {
        $updateArray = Utilities::filterArray($updateArray, ['lastName', 'firstName']);
        \core\Core::getInstance()->db->update(self::$tableName, $updateArray, ['id' => $id]);
    }
    public static function deleteUser($id)
    {
        \core\Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
    }
    public static function getUser($email)
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', ['login' => $email]);
        return !empty($user) ? $user[0] : null;
    }

    public static function isSuchUser($login)
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', ['login' => $login]);
        return !empty($user) ? true : false;
    }
    public static function loginUser($login, $password)
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => $password

        ]);
        return !empty($user) ? true : false;
    }

    public static function getLoginAndPassword($login, $password)
    {
        $user = \core\Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => $password
        ]);
        return $user;
    }

    public static function authUser($user)
    {
        $_SESSION['user'] = array_shift($user);
    }
    public static function logoutUser()
    {
        unset($_SESSION['user']);
    }
    public static function isAuth()
    {
        return isset($_SESSION['user']);
    }
    public static function getAuthUser()
    {
        return $_SESSION['user'];
    }
    public static function updateUserAvatar($id, $avatar)
    {
        \core\Core::getInstance()->db->update(self::$tableName, ['avatar' => $avatar], ['id' => $id]);
    }
    public static function updateUserInfo($id, $lastName, $firstName, $phone)
    {
        \core\Core::getInstance()->db->update(self::$tableName, ['firstName' => $firstName, 'lastName' => $lastName, 'phone' => $phone], ['id' => $id]);
    }

    public static function deleteProfileAvatar($id)
    {
        \core\Core::getInstance()->db->update(self::$tableName, ['avatar' => null], ['id' => $id]);
    }
   
}
