<?php

namespace models;

class Products
{
    protected static $tableName = 'product';
    public static function getProducts($category_id, $type = null, $orderBy = null)
    {
        if ($type) {
            $products = \core\Core::getInstance()->db->select(self::$tableName, '*', ['category_id' => $category_id, 'type' => $type], $orderBy);
            return $products;
        } else {
            $products = \core\Core::getInstance()->db->select(self::$tableName, '*', ['category_id' => $category_id], $orderBy);
            return $products;
        }
    }
    public static function getProduct($id)
    {
        $product = \core\Core::getInstance()->db->select(self::$tableName, '*', ['id' => $id]);
        return array_shift($product);
    }
    public static function getAllProducts()
    {
        $products = \core\Core::getInstance()->db->select(self::$tableName, '*');
        return $products;
    }
    public static function getRatingProducts()
    {
        $products = \core\Core::getInstance()->db->select(self::$tableName, '*', ['rating' => 5]);
        return $products;
    }
    public static function removeProduct($id)
    {
        $result = \core\Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
        return $result;
    }
    public static function addProduct($name, $type, $descr, $price, $count, $rating, $category_id, $producer_id, $link)
    {
        $result = \core\Core::getInstance()->db->insert(self::$tableName, [
            'name' => $name,
            'type' => $type,
            'descr' => $descr,
            'price' => $price,
            'count' => $count,
            'rating' => $rating,
            'category_id' => $category_id,
            'producer_id' => $producer_id,
            'link' => $link
        ]);
        return $result;
    }

    public static function editProduct($id, $newName, $newType, $newDescr, $newPrice, $newCount, $newRating, $newCategoryId, $newProducerId, $newLink)
    {
        $result = \core\Core::getInstance()->db->update(self::$tableName, [
            'name' => $newName,
            'type' => $newType,
            'descr' => $newDescr,
            'price' => $newPrice,
            'count' => $newCount,
            'rating' => $newRating,
            'category_id' => $newCategoryId,
            'producer_id' => $newProducerId,
            'link' => $newLink
        ], ['id' => $id]);
        return $result;
    }

    public static function getProductTypes()
    {
        $types = \core\Core::getInstance()->db->select(self::$tableName, 'type, category_id' );
        $types = array_unique($types, SORT_REGULAR);

        return $types;
    }
}
