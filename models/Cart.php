<?php

namespace models;

class Cart
{
    public static function writeSessionCart($id, $name, $price, $count, $link)
    {
        if (isset($_SESSION['cart'][$id])) {
            $_SESSION['cart'][$id]['count'] += $count;
        } else {
            $_SESSION['cart'][$id] = [
                'id' => $id,
                'name' => $name,
                'price' => $price,
                'count' => $count,
                'link' => $link
            ];
        }
    }

    public static function buyProducts($shos)
    {
        foreach ($_SESSION['cart'] as $id => $product) {
            
            $product = \models\Products::getProduct($id);
            $count = $product['count'] - $shos[$id];
            \models\Products::editProduct($id, $product['name'], $product['type'], $product['descr'], $product['price'], $count, $product['rating'], $product['category_id'], $product['producer_id'], $product['link']);
        }
        unset($_SESSION['cart']);
        \core\Core::getInstance()->redirect('/user/cart');
    }
}
