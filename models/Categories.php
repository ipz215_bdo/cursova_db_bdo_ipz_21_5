<?php

namespace models;

class Categories
{
    protected static $tableName = 'category';
    public static function getCategories()
    {
        $categories = \core\Core::getInstance()->db->select(self::$tableName, '*');
        return $categories;
    }
    public static function getCategory($name)
    {
        $category = \core\Core::getInstance()->db->select(self::$tableName, '*', ['name' => $name]);
        return array_shift($category);
    }
    public static function getCategoryById($id)
    {
        $category = \core\Core::getInstance()->db->select(self::$tableName, '*', ['id' => $id]);
        return array_shift($category);
    }
    public static function removeCategory($id)
    {
        $result = \core\Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
        return $result;
    }
    public static function addCategory($name, $UkrName)
    {
        if (!self::getCategory($name)) {
            $result = \core\Core::getInstance()->db->insert(self::$tableName, ['name' => $name, 'UkrName' => $UkrName]);
            return $result;
        } else {
            return false;
        }
    }

    public static function editCategory($id, $newName, $newUkrName)
    {
        $result = \core\Core::getInstance()->db->update(self::$tableName, ['name' => $newName, 'UkrName' => $newUkrName], ['id' => $id]);
        return $result;
    }
}
