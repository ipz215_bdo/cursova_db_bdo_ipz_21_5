<?php

namespace models;

class Producer
{
    protected static $tableName = 'producer';
    public static function getProducers()
    {
        $producers = \core\Core::getInstance()->db->select(self::$tableName, '*');
        return $producers;
    }
    public static function getProducer($name)
    {
        $producer = \core\Core::getInstance()->db->select(self::$tableName, '*', ['name' => $name]);
        return array_shift($producer);
    }
    public static function getProducerById($id)
    {
        $producer = \core\Core::getInstance()->db->select(self::$tableName, '*', ['id' => $id]);
        return array_shift($producer);
    }
    public static function removeProducer($id)
    {
        $result = \core\Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
        return $result;
    }

    public static function addProducer($name)
    {
        if (!self::getProducer($name)) {
            $result = \core\Core::getInstance()->db->insert(self::$tableName, ['name' => $name]);
            return $result;
        } else {
            return false;
        }
    }
    
    public static function editProducer($id, $newName)
    {
        $result = \core\Core::getInstance()->db->update(self::$tableName, ['name' => $newName], ['id' => $id]);
        return $result;
    }
}
