<?php

/**
 * @var string|null $error
 * @var array $model
 */
?>

<div class="container" style="margin-top:200px;">
    <h1>Вхід</h1>
    <form action="" method="post" style="margin-top:50px;">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Логін</label>
            <input type="email" class="form-control" name="login" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?= $model['login'] ?>">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Пароль</label>
            <input type="password" class="form-control" name="password" id="exampleInputPassword1" value="<?= $model['password'] ?>">
        </div>
        <button type="submit" class="btn btn-danger">Ввійти</button>
        <?php if (!empty($error)) : ?>
            <span class="error">
                <?= $error; ?>
            </span>
        <?php endif; ?>
    </form>
</div>