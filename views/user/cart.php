<section class="h-100 mt-2" style="background-color: #eee;">
    <form action="" method="POST">
        <div class="container h-100 py-5">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-10">
                    <div class="d-flex justify-content-center mt-4 align-items-center mb-4">
                        <h3 class="fw-normal mb-0 text-black">Кошик замовлень</h3>
                    </div>
                    <?php if (!empty($_SESSION['cart'])) : ?>
                        <?php foreach ($_SESSION['cart'] as $cart) : ?>
                            <div class="card rounded-3 mb-4">
                                <div class="card-body p-4">
                                    <div class="row d-flex justify-content-between align-items-center">
                                        <div class="col-md-2 col-lg-2 col-xl-2">
                                            <img src="<?= $cart['link'] ?>" class="img-fluid rounded-3" alt="Cotton T-shirt">
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-xl-3">
                                            <p class="lead fw-normal mb-2">
                                                <?php
                                                if (strlen($cart['name']) > 20) {
                                                    echo substr($cart['name'], 0, 20) . "...";
                                                } else {
                                                    echo $cart['name'];
                                                }
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-xl-2 d-flex">
                                            <div class="btn btn-danger px-2 groupBtn mx-2" onclick="this.parentNode.querySelector('input[type=number]').stepDown()">
                                                -
                                            </div>

                                            <input id="form1" style="width:50px ;" min="1" name="<?= $cart['id'] ?>" value="<?= $cart['count'] ?>" type="number" class="form-control form-control-sm quantity" />

                                            <div class="btn btn-danger px-2 groupBtn mx-2" onclick="this.parentNode.querySelector('input[type=number]').stepUp()">
                                                +
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                                            <h5 class="mb-0 "><span class="product-price"><?= $cart['price'] ?></span> грн</h5>
                                        </div>
                                        <div class="col-md-1 col-lg-1 col-xl-1 text-end">
                                            <a href="/user/deleteCart/<?= $cart['id'] ?>" class="btn btn-danger flex-shrink-0">
                                                <img src="https://pixsector.com/cache/6ecfa54e/avd0879fcbf810d38dc8e.png" style="width: 30px;" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <?php echo "<h5 class='text-center'>Кошик порожній</h5>"; ?>
                    <?php endif; ?>


                    <?php
                    $total = 0;
                    if (!empty($_SESSION['cart'])) {
                        foreach ($_SESSION['cart'] as $cart) {
                            $total += $cart['price'] * $cart['count'];
                        }
                    }

                    if (isset($_POST['order'])) {
                        \models\Cart::buyProducts($_POST);
                    }
                    ?>
                    <div class="card">
                        <div class="card-body text-center">

                            <button type="submit" class="btn btn-danger btn-block btn-lg" name="order">Замовити</button>

                            <span class="price " style="color:black">Total: <span class="total-price"><?= $total ?></span> грн</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<script src="/views/user/cart.js"></script>