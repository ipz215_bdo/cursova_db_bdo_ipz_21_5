document.addEventListener('DOMContentLoaded', function () {
    function bindModal(triger, modal, close) {
        triger.addEventListener('click', (e) => {
            if (e.target) {
                e.preventDefault();
            }
            modal.style.display = 'block';
            document.body.style.overflow = 'hidden';
        });
        close.addEventListener('click', () => {
            modal.style.display = 'none';
            document.body.style.overflow = '';
        });
        modal.addEventListener('click', (e) => {
            if (e.target === modal) {
                modal.style.display = 'none';
                document.body.style.overflow = '';
            }
        });
    }

    // Profile modals
    const trigerImageProfile = document.querySelector('.callModalImage'),
        modalForImage = document.querySelector('.popup_image'),
        closeModalImage = document.querySelector('.popup_image .popup_close');

    const trigerBtnForInfo = document.querySelector('.callEditInfo'),
        modalInfo = document.querySelector('.popup_info'),
        closeModalInfo = document.querySelector('.popup_info .popup_close');


    bindModal(trigerImageProfile, modalForImage, closeModalImage);
    bindModal(trigerBtnForInfo, modalInfo, closeModalInfo);
});