function bindModal(triger, modal, close) {
    triger.addEventListener('click', (e) => {
        if (e.target) {
            e.preventDefault();
        }
        modal.style.display = 'block';
        document.body.style.overflow = 'hidden';
    });
    close.addEventListener('click', () => {
        modal.style.display = 'none';
        document.body.style.overflow = '';
    });
    modal.addEventListener('click', (e) => {
        if (e.target === modal) {
            modal.style.display = 'none';
            document.body.style.overflow = '';
        }
    });
}


// ADMIN PANEL
const btns = document.querySelectorAll('.showTables');
const tables = document.querySelectorAll('.table');

btns.forEach((item, i) => {
    item.addEventListener('click', () => {
        tables[i].classList.toggle('hidden');
        tables[i].classList.add('faded');

    });
});


const addCategoryBtn = document.querySelector('.actionWithCategory'),
    categoryPopup = document.querySelector('.popup_category'),
    closeCategory = document.querySelector('.popup_category .popup_close');

const addProductBtn = document.querySelector('.actionWithProduct'),
    productPopup = document.querySelector('.popup_product'),
    closeProduct = document.querySelector('.popup_product .popup_close');

const addProducerBtn = document.querySelector('.actionWithProducer'),
    producerPopup = document.querySelector('.popup_producer'),
    closeProducer = document.querySelector('.popup_producer .popup_close');


bindModal(addCategoryBtn, categoryPopup, closeCategory);
bindModal(addProductBtn, productPopup, closeProduct);
bindModal(addProducerBtn, producerPopup, closeProducer);