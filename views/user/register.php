<?php

/**
 * @var array $errors
 * @var array $model
 */
?>

<div class="container" style="margin-top:200px;">
    <h1>Реєстрація</h1>
    <form action="" method="post" style="margin-top:50px;">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Логін</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name="login" aria-describedby="emailHelp" value="<?= $model['login'] ?>">
            <?php if (!empty($errors['login'])) : ?>
                <span class="error"><?= $errors['login']; ?></span>
            <?php endif ?>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Пароль</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password" value="<?= $model['password'] ?>">
            <?php if (!empty($errors['password'])) : ?>
                <span class="error"><?= $errors['password']; ?></span>
            <?php endif ?>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Пароль(ще раз)</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password2" value="<?= $model['password'] ?>">
            <?php if (!empty($errors['password'])) : ?>
                <span class="error"><?= $errors['password']; ?></span>
            <?php endif ?>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Ім'я</label>
            <input type="text" class="form-control" id="exampleInputPassword1" name="firstName" value="<?= $model['firstName'] ?>">
            <?php if (!empty($errors['firstName'])) : ?>
                <span class="error"><?= $errors['firstName']; ?></span>
            <?php endif ?>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Фамілія</label>
            <input type="text" class="form-control" id="exampleInputPassword1" name="lastName" value="<?= $model['lastName'] ?>">
            <?php if (!empty($errors['lastName'])) : ?>
                <span class="error"><?= $errors['lastName']; ?></span>
            <?php endif ?>
        </div>
        <button type="submit" class="btn btn-danger">Зареєструватись</button>
    </form>
</div>