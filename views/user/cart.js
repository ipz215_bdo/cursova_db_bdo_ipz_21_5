const groupBtn = document.querySelectorAll('.groupBtn'),
    quantity = document.querySelectorAll('.quantity'),
    price = document.querySelectorAll('.product-price'),
    total = document.querySelector('.total-price');


groupBtn.forEach((button) => {
    button.addEventListener('click', () => {
        let totalPrice = 0;
        for (let i = 0; i < quantity.length; i++) {
            totalPrice += parseInt(quantity[i].value) * parseInt(price[i].innerHTML);
        }
        total.innerHTML = totalPrice;
    });
});

quantity.forEach((input) => {
    input.addEventListener('input', () => {
        input.value = input.value.replace(/\D/, '');
        if (input.value < 1) {
            input.value = 1;
        }
        let totalPrice = 0;
        for (let i = 0; i < quantity.length; i++) {
            totalPrice += parseInt(quantity[i].value) * parseInt(price[i].innerHTML);
        }
        total.innerHTML = totalPrice;
    });
});