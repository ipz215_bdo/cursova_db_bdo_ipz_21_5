<section class="adminPage mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-4">
                <h1>Адмінка</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="mt-3">Категорії</h2>
                <div class="row-butons d-flex mb-3">
                    <button class="btn btn-danger actionWithCategory">Додати категорію</button>
                    <button class="btn btn-danger mx-2 showTables">&darr;</button>
                </div>
                <table class="table table-striped text-center ">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Назва(Англ)</th>
                            <th scope="col">Назва(Укр)</th>
                            <th scope="col">Дії</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($categories as $category) : ?>
                            <tr>
                                <th scope="row"><?= $category['id'] ?></th>
                                <td><?= $category['name'] ?></td>
                                <td><?= $category['UkrName'] ?></td>
                                <td>
                                    <form action="" method="get">
                                        <a href="/edit/editCategory/<?= $category['id'] ?>" type="submit" class="btn btn-primary" name="editCategory">Редагувати</a>
                                        <a href="/user/deleteCategory/<?= $category['id'] ?>" type="submit" class="btn btn-danger" name="deleteCategory">Видалити</a>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="mt-3">Виробник</h2>
                <div class="row-butons d-flex mb-3">
                    <button class="btn btn-danger actionWithProducer">Додати виробника</button>
                    <button class="btn btn-danger mx-2 showTables">&darr;</button>
                </div>
                <table class="table table-striped text-center ">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Назва</th>
                            <th scope="col">Дії</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($producers as $producer) : ?>
                            <tr>
                                <th scope="row"><?= $producer['id'] ?></th>
                                <td><?= $producer['name'] ?></td>
                                <td>
                                    <form action="" method="get">

                                        <a href="/edit/editProducer/<?= $producer['id'] ?>" type="submit" class="btn btn-primary" name="editProducer">Редагувати</a>
                                        <a href="/user/deleteProducer/<?= $producer['id'] ?>" type="submit" class="btn btn-danger" name="deleteProducer">Видалити</a>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12">
                <h2>Товари</h2>
                <div class="row-butons d-flex mb-3">
                    <button class="btn btn-danger actionWithProduct">Додати товар</button>
                    <button class="btn btn-danger mx-2 showTables">&darr;</button>
                </div>
                <table class="table table-striped text-center table-products ">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Картинка</th>
                            <th scope="col">Назва</th>
                            <th scope="col">Тип продукту</th>
                            <th scope="col">Опис</th>
                            <th scope="col">Ціна</th>
                            <th scope="col">Кількість</th>
                            <th scope="col">Рейтинг</th>
                            <th scope="col">ID Категорії</th>
                            <th scope="col">ID Виробника</th>
                            <th scope="col">Дії</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product) : ?>
                            <tr>
                                <th scope="row"><?= $product['id'] ?></th>
                                <td><img src="<?= $product['link'] ?>" style="width: 50px; height: 70px;" alt="image"></td>
                                <td>
                                    <?php
                                    if (strlen($product['name']) > 40) {
                                        echo substr($product['name'], 0, 40) . '...';
                                    } else {
                                        echo $product['name'];
                                    } ?>
                                </td>
                                <td><?= $product['type'] ?></td>
                                <td>
                                    <?php
                                    if (strlen($product['descr']) > 50) {
                                        echo substr($product['descr'], 0, 50) . '...';
                                    } else {
                                        echo $product['descr'];
                                    }
                                    ?>
                                </td>
                                <td><?= $product['price'] ?></td>
                                <td><?= $product['count'] ?></td>
                                <td><?= $product['rating'] ?></td>
                                <td><?= $product['category_id'] ?></td>
                                <td><?= $product['producer_id'] ?></td>
                                <td>
                                    <form action="" method="get">
                                        <a href="/edit/editProduct/<?= $product['id'] ?>" type="submit" class="btn btn-primary" name="editProduct">Редагувати</a>
                                        <a href="/user/deleteProduct/<?= $product['id'] ?>" type="submit" class="btn btn-danger" style="width: 105px; margin-top:5px;" name="deleteProduct">Видалити</a>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- MODAL -->
    <div class="popup_category">
        <div class="popup_dialog">
            <div class="popup_content text-center">
                <button type="button" class="popup_close"><strong>&times;</strong></button>
                <div class="popup_form">
                    <form class="form" method="get">
                        <h2 class="mt-2">Додати категорію</h2>
                        <input class="form-control form_input" name="EngName" required type="text" placeholder="Введіть назву(англ)">
                        <input class="form-control form_input" name="UkrName" required type="text" placeholder="Введіть назву(укр)">
                        <button type="submit" class="text-uppercase btn btn-danger mb-3" name="submitCategory">Зберегти</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['submitCategory'])) {
        $name = $_GET['EngName'];
        $UkrName = $_GET['UkrName'];
        if (\models\Categories::addCategory($name, $UkrName)) {
            header('Location: /user/admin');
        } else {
            echo '<script>alert("Категорія з такою назвою вже існує")</script>';
        }
    }
    ?>

    <div class="popup_producer">
        <div class="popup_dialog">
            <div class="popup_content text-center">
                <button type="button" class="popup_close"><strong>&times;</strong></button>
                <div class="popup_form">
                    <form class="form" method="get">
                        <h2 class="mt-2">Додати виробника</h2>
                        <input class="form-control form_input" name="producerName" required type="text" placeholder="Введіть назву виробника">
                        <button type="submit" class="text-uppercase btn btn-danger mb-3" name="submitProducer">Зберегти</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['submitProducer'])) {
        $producerName = $_GET['producerName'];
        if (\models\Producer::addProducer($producerName)) {
            header('Location: /user/admin');
        } else {
            echo '<script>alert("Виробник з такою назвою вже існує")</script>';
        }
    }
    ?>

    <div class="popup_product">
        <div class="popup_dialog">
            <div class="popup_content text-center">
                <button type="button" class="popup_close"><strong>&times;</strong></button>
                <div class="popup_form">
                    <form class="form" method="get">
                        <h2 class="mb-3 mt-2">Додати продукт</h2>
                        <input class="form-control form_input" name="product_image" required type="text" placeholder="Вставте посилання на картинку">
                        <input class="form-control form_input" name="product_name" required type="text" placeholder="Введіть назву продукту">
                        <input class="form-control form_input" name="product_type" required type="text" placeholder="Введіть тип продукту">
                        <input class="form-control form_input" name="product_price" required type="number" placeholder="Введіть ціну продукту">
                        <input class="form-control form_input" name="product_rating" type="number" placeholder="Введіть рейтинг продукту">
                        <input class="form-control form_input" name="product_count" type="number" placeholder="Введіть кількість продукту">
                        <input class="form-control form_input" name="product_descr" type="text" placeholder="Введіть опис продукту">
                        <input class="form-control form_input" name="product_category" required type="number" min="0" placeholder="Введіть id категорії">
                        <input class="form-control form_input" name="product_producer" required type="number" placeholder="Введіть id виробника">
                        <button class="text-uppercase btn btn-danger mb-3" name="submitProduct">Зберегти</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['submitProduct'])) {
        $image = $_GET['product_image'];
        $name = $_GET['product_name'];
        $type = $_GET['product_type'];
        $price = $_GET['product_price'];
        $rating = $_GET['product_rating'] ? $_GET['product_rating'] : 0;
        $count = $_GET['product_count'] ? $_GET['product_count'] : 0;
        $descr = $_GET['product_descr'] ? $_GET['product_descr'] : 'Empty';
        $category = $_GET['product_category'];
        $producer = $_GET['product_producer'];
        \models\Products::addProduct($name, $type, $descr, $price, $count, $rating, $category, $producer, $image);
        header('Location: /user/admin');
    }
    ?>
</section>


<script src="/views/user/admin.js"></script>