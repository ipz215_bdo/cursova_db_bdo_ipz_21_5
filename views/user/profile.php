<section style="background-color: #eee;">
  <div class="container py-5">
    <div class="row">
      <div class="col-lg-4">
        <div class="card mb-4">
          <div class="card-body text-center">
            <img src="
            <?php
            if ($user['avatar'] == null) {
              echo 'https://www.freeiconspng.com/thumbs/profile-icon-png/am-a-19-year-old-multimedia-artist-student-from-manila--21.png';
            } else {
              echo $user['avatar'];
            }
            ?>
            " alt="avatar" class="rounded-circle img-fluid" style="width: 150px;">
            <h5 class="my-3">
              <?= strtoupper($user['lastName'] . ' ' . $user['firstName']) ?>
            </h5>
            <div class="d-flex justify-content-center mb-2 mt-4">
              <a type="button" class="btn btn-danger text-uppercase fw-bold callModalImage">Змінити</a>
              <a href="/user/deleteProfileAvatar/<?= $user['id'] ?>" type="submit" class="btn btn-danger ms-1 text-uppercase fw-bold" name="deleteImage">Видалити</a>
            </div>
          </div>
        </div>
        <div class="card mb-4 mb-lg-0">
          <div class="card-body p-0">
            <ul class="list-group list-group-flush rounded-3">
              <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                <i class="fas fa-globe fa-lg text-warning"></i>
                <p class="mb-0">https://mdbootstrap.com</p>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                <i class="fab fa-github fa-lg" style="color: #333333;"></i>
                <p class="mb-0">mdbootstrap</p>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                <i class="fab fa-twitter fa-lg" style="color: #55acee;"></i>
                <p class="mb-0">@mdbootstrap</p>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                <i class="fab fa-instagram fa-lg" style="color: #ac2bac;"></i>
                <p class="mb-0">mdbootstrap</p>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center p-3">
                <i class="fab fa-facebook-f fa-lg" style="color: #3b5998;"></i>
                <p class="mb-0">mdbootstrap</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-5">
                <p class="mb-0">Ім'я</p>
              </div>
              <div class="col-sm-7">
                <p class="text-muted mb-0"><?= $user['firstName'] ?></p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-5">
                <p class="mb-0">Призвіще</p>
              </div>
              <div class="col-sm-7">
                <p class="text-muted mb-0"><?= $user['lastName'] ?></p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-5">
                <p class="mb-0">Пошта</p>
              </div>
              <div class="col-sm-7">
                <p class="text-muted mb-0"><?= $user['login'] ?></p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-5">
                <p class="mb-0">Мобільний номер</p>
              </div>
              <div class="col-sm-7">
                <p class="text-muted mb-0">
                  <?php
                  if ($user['phone'] == null) {
                    echo 'порожньо';
                  } else {
                    echo $user['phone'];
                  }
                  ?>
                </p>
              </div>
            </div>
            <hr>
            <div class="row">
              <button type="button" class="btn btn-danger callEditInfo">
                Змінити
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modals -->

  <div class="popup_image">
    <div class="popup_dialog">
      <div class="popup_content text-center">
        <button type="button" class="popup_close"><strong>&times;</strong></button>
        <div class="popup_form">
          <form class="form">
            <h2 class="mb-3 mt-2">Заміна картинки профілю</h2>
            <form action="" method="get">
              <input class="form-control form_input" name="user_avatar" required type="text" placeholder="Вставте посилання на картинку">
              <button class="text-uppercase btn btn-danger mb-3" name="save_image">Зберегти</button>
            </form>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="popup_info">
    <div class="popup_dialog">
      <div class="popup_content_info text-center">
        <button type="button" class="popup_close"><strong>&times;</strong></button>
        <div class="popup_form">
          <form class="form">
            <h2 class="mt-2">Заміна даних профілю</h2>
            <input class="form-control form_input" name="user_name" type="text" placeholder="Введіть ваше ім'я" value="<?= $user['firstName'] ?>">
            <input class="form-control form_input" name="user_surname" type="text" placeholder="Введіть ваше призвіще" value="<?= $user['lastName'] ?>">
            <input class="form-control form_input" name="user_phone" type="text" placeholder="Введіть ваш мобільний номер" value="<?= $user['phone'] ?>">
            <button class="text-uppercase btn btn-danger mb-3" name="save_profile_info">Зберегти</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <?php
  if (isset($_GET['save_image'])) {
    $user_avatar = $_GET['user_avatar'];
    $user_id = $_SESSION['user']['id'];
    $_SESSION['user']['avatar'] = $user_avatar;
    \models\User::updateUserAvatar($user_id, $user_avatar);
    header('Location: /user/profile');
  }

  if (isset($_GET['save_profile_info'])) {
    $user_name = $_GET['user_name'] ? $_GET['user_name'] : $user['firstName'];
    $user_surname = $_GET['user_surname'] ? $_GET['user_surname'] : $user['lastName'];
    $user_phone = $_GET['user_phone'] ? $_GET['user_phone'] : $user['phone'];
    $user_id = $_SESSION['user']['id'];

    $_SESSION['user']['firstName'] = $user_name;
    $_SESSION['user']['lastName'] = $user_surname;
    $_SESSION['user']['phone'] = $user_phone;
    
    \models\User::updateUserInfo($user_id, $user_name, $user_surname, $user_phone);
    header('Location: /user/profile');
  }

  ?>
</section>

<script src="/views/user/profile.js"></script>