<section class="promo">
    <div class="container">
        <div class="promo_descr">
            <div class="row">
                <div class="col-12">
                    <svg width="800" height="200" xmlns="http://www.w3.org/2000/svg">
                        <rect width="800" height="200" fill="white" />
                        <text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" fill="#800080" font-size="48" font-family="'Comic Sans MS', cursive, sans-serif" stroke="#fff" stroke-width="2">
                            Еліксир Смаку
                        </text>
                    </svg>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-6" style="font-family: 'Comic Sans MS';">
                    <p>Ласкаво просимо до світу неперевершених смаків та ароматів, де кожна пляшка розкриває перед вами двері в унікальні винні погреби, пивоварні та дистилерії світу. У нашому магазині "Еліксир Смаку" ми прагнемо пропонувати вам лише найкраще з найкращого — алкогольну продукцію, яка долає час та кордони, даруючи незабутні враження.</p>
                    <p>Зануртесь у дивовижну атмосферу нашого магазину, де ви зможете знайти:</p>
                    <ul>
                        <li>
                            <p>Ексклюзивні Вина: Від класичних червоних та білих вин до преміальних колекцій із всього світу.</p>
                        </li>
                        <li>
                            <p>Широкий Вибір Спиртних Напоїв: Чи це класичний коньяк, ароматний ром, або добірний віскі — у нас є все для цінителів доброго алкоголю.</p>
                        </li>
                        <li>
                            <p>Різноманітність Пива: Від знаних брендів до крафтових новинок.</p>
                        </li>
                        <li>
                            <p>Експертна Консультація: Наші досвідчені консультанти завжди готові допомогти вам у виборі та розповісти про особливості кожного продукту.</p>
                        </li>
                    </ul>
                    <p>Запрошуємо вас у подорож смаками та ароматами світу разом з "Еліксиром Смаку". З нами кожна покупка перетворюється на справжнє свято для гурманів.</p>
                    <p>
                        Відкрийте для себе насолоду вишуканого вибору в "Еліксирі Смаку"!
                    </p>
                    <p>
                    З найкращими побажаннями,
                    Команда "Еліксир Смаку"
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="recommended_products">
    <div class="container">
        <div class="row">
            <div class="recomendation_text">
                <h2>Рекомендовані товари</h2>
            </div>
            <?php foreach ($products as $product) : ?>

                <div class="col-md-3">
                    <div class="card w-auto">
                        <div class="rating" data-total-value="<?= $product['rating']; ?>">
                            <div class="rating_item" data-item-value="5">★</div>
                            <div class="rating_item" data-item-value="4">★</div>
                            <div class="rating_item" data-item-value="3">★</div>
                            <div class="rating_item" data-item-value="2">★</div>
                            <div class="rating_item" data-item-value="1">★</div>
                        </div>
                        <a href="/products/view/<?= $product['id'] ?>">
                            <img style="width: 170px; height: 170px;" src="<?= $product['link'] ?>" alt="" class="image">
                        </a>
                        <div class="card_descr">
                            <div class="name">
                                <?php
                                    echo $product['name'];
                                ?>
                            </div>
                            <div class="price"><?= $product['price'] ?> грн</div>
                        </div>
                        <a href="/products/addProductToCart/<?= $product['id'] ?>/<?= $category['name'] ?>" class="btn btn-danger w-100 text-uppercase fw-medium">Додати в кошик</a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>