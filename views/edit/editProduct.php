<div class="container" style="margin-top: 300px;">
    <form action="" method="post">
        <label for="">Name</label>
        <input type="text" class="form-control" name="name" value="<?= $product['name'] ?>">

        <label for="">Type</label>
        <input type="text" class="form-control" name="type" value="<?= $product['type'] ?>">

        <label for="">Description</label>
        <input type="text" class="form-control" name="descr" value="<?= $product['descr'] ?>">

        <label for="">Price</label>
        <input type="text" class="form-control" name="price" value="<?= $product['price'] ?>">

        <label for="">Count</label>
        <input type="text" class="form-control" name="count" value="<?= $product['count'] ?>">

        <label for="">Rating</label>
        <input type="text" class="form-control" name="rating" value="<?= $product['rating'] ?>">

        <label for="">Category ID</label>
        <input type="text" class="form-control" name="category_id" value="<?= $product['category_id'] ?>">

        <label for="">Producer ID</label>
        <input type="text" class="form-control" name="producer_id" value="<?= $product['producer_id'] ?>">

        <label for="">Link</label>
        <input type="text" class="form-control" name="link" value="<?= $product['link'] ?>">
        <input type="submit" class="btn btn-danger mt-2" name="submit" value="Зберегти">
    </form>
</div>