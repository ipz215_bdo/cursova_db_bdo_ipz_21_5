<section class="disposables">
    <div class="container">
        <div class="row">
            <div class="panels">
                <div class="disposable_text text-uppercase">
                    <h2><?= $category['UkrName'] ?></h2>
                </div>
                <form action="" method="get">
                    <div class="btn-group">
                        <select name="sort" class="form-select">
                            <option disabled selected>Сортування</option>
                            <option value="Order by name">Назва(А-Я)</option>
                            <option value="Order by name desc">Назва(Я-А)</option>
                            <option value="Order by price">Ціна(Зростанням)</option>
                            <option value="Order by price desc">Ціна(Спаданням)</option>
                            <option value="Order by rating">Рейтинг(Зростанням)</option>
                            <option value="Order by rating desc">Рейтинг(Спаданням)</option>
                        </select>
                    </div>
                    <div class="btn-group">
                        <select name="type" class="form-select">
                            <option disabled selected>
                                <?= "Тип" ?>
                            </option>
                            <?php foreach ($types as $type) : ?>
                                <? if ($type['category_id'] == $category['id']) : ?>
                                    <option value="<?= $type['type'] ?>"><?= $type['type'] ?></option>
                                <? endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-danger">
                </form>
            </div>
            <?php foreach ($products as $product) : ?>
                <div class="col-md-3">
                    <form action="" method="get">
                        <div class="card">
                            <div class="rating" data-total-value="<?= $product['rating']; ?>">
                                <div class="rating_item" data-item-value="5">★</div>
                                <div class="rating_item" data-item-value="4">★</div>
                                <div class="rating_item" data-item-value="3">★</div>
                                <div class="rating_item" data-item-value="2">★</div>
                                <div class="rating_item" data-item-value="1">★</div>
                            </div>

                            <a href="/products/view/<?= $product['id'] ?>">
                                <img style="width: 170px; height: 170px;" src="<?= $product['link'] ?>" alt="">
                            </a>
                            <div class="card_descr">
                                <div class="name">
                                    <?php
                                    echo $product['name'];
                                    ?>
                                </div>
                                <div class="price"><?= $product['price'] ?> грн</div>
                            </div>
                            <a href="/products/addProductToCart/<?= $product['id'] ?>/<?= $category['name'] ?>" type="submit" class="btn btn-danger w-100 text-uppercase fw-medium">Додати в кошик</a>
                        </div>
                    </form>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<script src="index.js"></script>