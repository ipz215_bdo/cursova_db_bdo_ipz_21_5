<section class="py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="row gx-4 gx-lg-5 align-items-center">
            <div class="col-md-6"><img class="card-img-top mb-5 mb-md-0" src="<?= $product['link'] ?>" alt="..." /></div>
            <div class="col-md-6">
                <div class="rating" data-total-value="<?= $product['rating']; ?>">
                    <div class="rating_item" data-item-value="5">★</div>
                    <div class="rating_item" data-item-value="4">★</div>
                    <div class="rating_item" data-item-value="3">★</div>
                    <div class="rating_item" data-item-value="2">★</div>
                    <div class="rating_item" data-item-value="1">★</div>
                </div>
                <div class="small mb-1">код товару: <?= $product['id'] ?></div>
                <h1 class="display-5 fs-3 fw-bolder"><?= $product['name'] ?></h1>
                <div class="small mb-1">
                    <?php
                    if ($product['count'] > 10) {
                        echo "У наявності більше 10 шт.";
                    } else if($product['count'] < 1){
                        echo "Немає в наявності";
                    }else {
                        echo "У наявності " . $product['count'] . " шт.";
                    }
                    ?>
                </div>
                <div class="fs-5 mb-3">
                    <span><?= $product['price'] ?> грн</span>
                </div>

                <div class="d-flex mb-3">
                    <form action="" method="get" class="d-flex">
                        <input class="form-control text-center me-3" id="inputQuantity" min="1" name="inputQuantity" type="number" value="1" style="max-width: 3rem" />
                        <button class="btn btn-outline-dark flex-shrink-0" type="submit" name="addProduct">
                            <i class="bi-cart-fill me-1"></i>
                            Додати в кошик
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <?php
        if (isset($_GET['addProduct'])) {
            $id = $product['id'];
            $name = $product['name'];
            $price = $product['price'];
            $count = $_GET['inputQuantity'];
            $link = $product['link'];
            \models\Cart::writeSessionCart($id, $name, $price, $count, $link);
            header("Location: /products/view/$id");
        }
        ?>



        <div class="gx-4 gx-lg-5 align-items-center">
            <div class="col-md-6">
                <div class="fs-4 mb-3 mt-5">- Опис товару</div>
                <div class="fs-5 mb-3">
                    <span style="color: black ;">
                        <?php
                        echo $product['descr'];
                        ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>