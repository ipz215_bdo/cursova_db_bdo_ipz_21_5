<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="https://icons8.ru/icon/Ji3hqTT74sRw/vape">
    <title>Vape Shop RustyCoil</title>
    <link rel="stylesheet" href="/themes/light/rating.css">
    <link rel="stylesheet" href="/themes/light/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body>

    <header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary fixed-top" style="background-color: #e3f2fd;">
            <div class="container-fluid col-9">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="/main">Еліксир <span>Смаку</span></a>
                    <ul class="nav justify-content-center">
                        <?php
                        if (count($categories) < 5) {
                            foreach ($categories as $category) {
                                echo '<li class="nav-item mx-2">
                                        <a class="nav-link text-dark" href="/products/index/' . $category['name'] . '">' . $category['UkrName'] . '</a>
                                    </li>';
                            }
                        } else if (count($categories) >= 5) {
                            for ($i = 0; $i < 4; $i++) {
                                echo '<li class="nav-item mx-2">
                                        <a class="nav-link text-dark" href="/products/index/' . $categories[$i]['name'] . '">' . $categories[$i]['UkrName'] . '</a>
                                    </li>';
                            }
                            echo '<li class="nav-item mx-2 dropdown">
                                    <a class="nav-link text-dark dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Більше
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">';
                            for ($i = 4; $i < count($categories); $i++) {
                                echo '<li><a class="dropdown-item" href="/products/index/' . $categories[$i]['name'] . '">' . $categories[$i]['UkrName'] . '</a></li>';
                            }
                            echo '</ul>
                                </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="dropdown text-end">
                    <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle show" data-bs-toggle="dropdown" aria-expanded="true">
                        <img class="profileImg" src="
                        <?php
                        if ($_SESSION['user']['avatar'] == null || !isset($_SESSION['user'])) {
                            echo 'https://www.freeiconspng.com/thumbs/profile-icon-png/am-a-19-year-old-multimedia-artist-student-from-manila--21.png';
                        } else {
                            echo $_SESSION['user']['avatar'];
                        }

                        ?>
                        " alt="mdo" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu" style="position: absolute; inset: 0px 0px auto auto; margin: 0px; transform: translate(45px, 43px);" data-popper-placement="bottom-end">
                        <?php
                        if (isset($_SESSION['user'])) {
                            if ($_SESSION['user']['login'] == 'ipz215_bdo@student.ztu.edu.ua') {
                                echo '<li><a class="dropdown-item" href="/user/admin">Адмінка</a></li>';
                            }
                            echo '<li><a class="dropdown-item" href="/user/cart">Кошик</a></li>
                            <li><a class="dropdown-item" href="/user/profile">Профіль</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="/user/logout">Вихід</a></li>';
                        } else {
                            echo '<li><a class="dropdown-item" href="/user/login">Увійти</a></li>';
                            echo '<li><a class="dropdown-item" href="/user/register">Зареєструватися</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </header>


    <main>
        <?= $content; ?>
    </main>


    <footer class="text-center text-lg-start bg-white text-muted w-100">
        <section class="footer">
            <div class="container text-center text-md-start mt-5 border-top">
                <div class="row mt-4">
                    <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                        <h6 class="text-uppercase fw-bold mb-4">
                            Продукти
                        </h6>
                        <?php foreach ($categories as $category) : ?>
                            <p>
                                <a class="text-reset" href="/products/index/<?= $category['name'] ?>"><?= $category['UkrName'] ?></a>
                            </p>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                        <h6 class="text-uppercase fw-bold mb-4">
                            Корисні посилання
                        </h6>
                        <p>
                            <a href="/user/profile" class="text-reset">Профіль</a>
                        </p>
                        <p>
                            <a href="/user/cart" class="text-reset">Кошик</a>
                        </p>
                    </div>
                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                        <h6 class="text-uppercase fw-bold mb-4">Контакти</h6>
                        <p>
                            <i class="fas fa-envelope me-3 text-secondary"></i>
                            ipz215_bdo@student.ztu.edu.ua
                        </p>
                        <p><i class="fas fa-phone me-3 text-secondary"></i>+380995336538</p>
                    </div>
                </div>
            </div>
        </section>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>