<?php

namespace controllers;

class EditController extends \core\Controller
{

    public function editCategoryAction($params)
    {
        $id = array_shift($params);
        $category =  \models\Categories::getCategoryById($id);

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $UkrName = $_POST['UkrName'];
            \models\Categories::editCategory($id, $name, $UkrName);
            header("Location: /user/admin");
        }
        return $this->render(null, ['category' => $category]);
    }

    public function editProductAction($params)
    {
        $id = array_shift($params);
        $product =  \models\Products::getProduct($id);
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $type = $_POST['type'];
            $price = $_POST['price'];
            $description = $_POST['descr'];
            $producer = $_POST['producer_id'];
            $category = $_POST['category_id'];
            $image = $_POST['link'];
            $count = $_POST['count'];
            $rating = $_POST['rating'];
            \models\Products::editProduct($id, $name, $type, $description, $price, $count, $rating, $category, $producer,  $image);
            header("Location: /user/admin");
        }
        return $this->render(null, ['product' => $product]);
    }

    public function editProducerAction($params)
    {
        $id = array_shift($params);
        $producer =  \models\Producer::getProducerById($id);
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            \models\Producer::editProducer($id, $name);
            header("Location: /user/admin");
        }
        return $this->render(null, ['producer' => $producer]);
    }
}
