<?php

namespace controllers;

class MainController extends \core\Controller
{
    public function indexAction()
    {
        $products = \models\Products::getRatingProducts();
        $products = array_slice($products, 0, 8);
        return $this->render(null,  ['products' => $products]);
    }
    public function errorAction($code)
    {
        switch ($code) {
            case 404: echo '404';
            break;
            
            default: echo 'Error';
        }
    }
}
