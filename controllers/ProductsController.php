<?php

namespace controllers;

class ProductsController extends \core\Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function viewAction($params)
    {
        $product = \models\Products::getProduct(array_shift($params));
        return $this->render(null, ['product' => $product]);
    }

    public function indexAction($params)
    {
        $category = \models\Categories::getCategory(array_shift($params));
        $categories = \models\Categories::getCategories();
        $products = \models\Products::getProducts($category['id'], $_GET['type'], $_GET['sort']);
        $productsGetTypes = \models\Products::getProducts($category['id']);
        $productTypes = \models\Products::getProductTypes();
        return $this->render(null, [
            'products' => $products,
            'productsGetTypes' => $productsGetTypes,
            'types' => $productTypes,
            'category' => $category,
            'categories' => $categories
        ]);
    }
    public function addProductToCartAction($params)
    {
        $product = \models\Products::getProduct(array_shift($params));
        $id = $product['id'];
        $name = $product['name'];
        $price = $product['price'];
        $link = $product['link'];
        $count = 1;
        $url = $_SERVER['HTTP_REFERER'];
        \models\Cart::writeSessionCart($id, $name, $price, $count, $link);
        \core\Core::getInstance()->redirect($url);
    }
}
