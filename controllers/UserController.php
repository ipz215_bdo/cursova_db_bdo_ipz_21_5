<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\User;
use core\Utilities;

class UserController extends Controller
{
    public function profileAction()
    {
        $user = User::getAuthUser();
        return $this->render(null, [
            'user' => $user
        ]);
    }

    public function adminAction()
    {
        if (User::getAuthUser()['login'] == 'ipz215_bdo@student.ztu.edu.ua') {
            $products = \models\Products::getAllProducts();
            $categories = \models\Categories::getCategories();
            $producers = \models\Producer::getProducers();
            return $this->render(null, [
                'products' => $products,
                'categories' => $categories,
                'producers' => $producers
            ]);
        } else {
            Core::getInstance()->redirect('/');
        }
    }

    public function registerAction()
    {
        if (User::getAuthUser()) {
            Core::getInstance()->redirect('/');
        } else {
            if (Core::getInstance()->requestMethod === 'POST') {
                $errors = [];
                if (!filter_var($_POST['login'], FILTER_VALIDATE_EMAIL)) {
                    $errors['login'] = 'Некоректний email';
                }
                if (User::isSuchUser($_POST['login'])) {
                    $errors['login'] = 'Такий email вже зареєстрований';
                }
                if (strlen($_POST['password']) < 8) {
                    $errors['password'] = 'Пароль має бути не менше 8 символів';
                }
                if ($_POST['password'] !== $_POST['password2']) {
                    $errors['password2'] = 'Паролі не співпадають';
                }
                if ($_POST['lastName'] === '') {
                    $errors['lastName'] = 'Введіть прізвище';
                }
                if ($_POST['firstName'] === '') {
                    $errors['firstName'] = 'Введіть ім\'я';
                }

                if (count($errors) > 0) {
                    $model = [
                        'login' => $_POST['login'],
                        'lastName' => $_POST['lastName'],
                        'firstName' => $_POST['firstName']
                    ];
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model
                    ]);
                } else {
                    $password = Utilities::Encrypt($_POST['password']);
                    User::addUser($_POST['login'], $password, $_POST['lastName'], $_POST['firstName']);
                    User::authUser(User::getLoginAndPassword($_POST['login'], $password));
                    return $this->renderPath('registerSuccess');
                }
            } else {
                return $this->render();
            }
        }
    }

    public function loginAction()
    {
        if (User::getAuthUser()) {
            Core::getInstance()->redirect('/');
        } else {
            if (User::getAuthUser()) {
                Core::getInstance()->redirect('/');
            }
            if (Core::getInstance()->requestMethod === "POST") {
                $password = Utilities::Encrypt($_POST['password']);
                $user = User::getLoginAndPassword($_POST['login'], $password);
                $error = null;
                if (empty($user)) {
                    $error = 'Невірний логін або пароль';
                } else {
                    User::authUser($user);
                    Core::getInstance()->redirect('/');
                }
            }

            return $this->render(null, [
                'error' => $error
            ]);
        }
    }

    public function logoutAction()
    {
        User::logoutUser();
        Core::getInstance()->redirect('/');
    }


    public function cartAction()
    {
        return $this->render();
    }

    public function deleteCategoryAction($params)
    {
        $id = array_shift($params);
        \models\Categories::removeCategory($id);
        Core::getInstance()->redirect('/user/admin');
    }
    public function deleteProductAction($params)
    {
        $id = array_shift($params);
        \models\Products::removeProduct($id);
        Core::getInstance()->redirect('/user/admin');
    }

    public function deleteProducerAction($params)
    {
        $id = array_shift($params);
        \models\Producer::removeProducer($id);
        Core::getInstance()->redirect('/user/admin');
    }

    public function deleteProfileAvatarAction($params)
    {
        $id = array_shift($params);
        $_SESSION['user']['avatar'] = null;
        User::deleteProfileAvatar($id);
        Core::getInstance()->redirect('/user/profile');
    }

    public function deleteCartAction($params)
    {
        $id = array_shift($params);
        unset($_SESSION['cart'][$id]);
        Core::getInstance()->redirect('/user/cart');
    }
}
